var Book = require('../models/book');
var Author = require('../models/author');
var Genre = require('../models/genre');
var BookInstance = require('../models/bookinstance');

exports.bookinstance_list = function(req, res){
	BookInstance.find().populate('book').exec(
		function (err, list_bookinstance){
			if (err) {return next(err);}
			res.render('bookinstance_list', {title: 'Book Instance List', bookinstance_list: list_bookinstance});
		}
	)
};

exports.bookinstance_detail = function(req, res){
	res.send('NOT IMPLEMENTED: BookInstance Detail');
};

exports.bookinstance_create_get = function(req, res){
	res.send('NOT IMPLEMENTED: BookInstance Create Get');
};

exports.bookinstance_create_post = function(req, res){
	res.send('NOT IMPLEMENTED: BookInstance Create Post');
};

exports.bookinstance_delete_get = function(req, res){
	res.send('NOT IMPLEMENTED: BookInstance Delete Get');
};

exports.bookinstance_delete_post = function(req, res){
	res.send('NOT IMPLEMENTED: BookInstance Delete Post');
};

exports.bookinstance_update_get = function(req, res){
	res.send('NOT IMPLEMENTED: BookInstance Update Get');
};

exports.bookinstance_update_post = function(req, res){
	res.send('NOT IMPLEMENTED: BookInstance Update Post');
};
