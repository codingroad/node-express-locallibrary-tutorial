var Book = require('../models/book');
var Author = require('../models/author');
var Genre = require('../models/genre');
var BookInstance = require('../models/bookinstance');

var async = require('async');

exports.index = function(req, res){

	async.parallel(
		{
			book_count: function(callback){
				Book.count(callback);
			},
			book_instance_count: function(callback){
				BookInstance.count(callback);
			},
			book_instance_available_count: function(callback){
				BookInstance.count({status: 'Availabel'}, callback);
			},
			author_count: function(callback){
				Author.count(callback);
			},
			genre_count: function(callback){
				Genre.count(callback);
			},
		}, function(err, result){
			res.render('index', {title: 'Local Library Home', error: err, data: result})
		}
	);
};

exports.book_list = function(req, res){
	Book.find({}, 'title author').populate('author').exec(
		function (err, list_books){
			if (err) {return next(err);}
			res.render('book_list', {title: 'Book List', book_list: list_books});
		}
	)
};

exports.book_detail = function(req, res){
	res.send('NOT IMPLEMENTED: Book Detail, BookID=' + req.params.id); 
};

exports.book_create_get = function(req, res){
	res.send('NOT IMPLEMENTED: Book Create Get');
};

exports.book_create_post = function(req, res){
	res.send('NOT IMPLEMENTED: Book Create Post');
};

exports.book_delete_get = function(req, res){
	res.send('NOT IMPLEMENTED: Book Delete Get');
};

exports.book_delete_post = function(req, res){
	res.send('NOT IMPLEMENTED: Book Delete Post');
};

exports.book_update_get = function(req, res){
	res.send('NOT IMPLEMENTED: Book Update Get');
};

exports.book_update_post = function(req, res){
	res.send('NOT IMPLEMENTED: Book Update Post');
};
