
## 记录

### 测试提交schema到mongodb

    
    ```
    curl 'https://raw.githubusercontent.com/hamishwillee/express-locallibrary-tutorial/master/populatedb.js' -o populatedb.js
    node populatedb mongodb://192.168.2.180:27017/my_database

    node populatedb mongodb://cd:123456@ds133746.mlab.com:33746/codingroad
    ```


### async

1. parallel 并行执行函数数组，将结果数据封装成map
2. series 串行执行函数数组
3. waterfall 串行执行函数数组，但是后面的函数可能需要用到前面函数调用结果做为入参

### mongodb操作

#### count
   
```
    BookInstance.count({status: 'Availabel'}, callback);
    Author.count(callback);
```
#### 关联查询 
```
Book.find({}, 'title author')
    .populate('author')
    .exec(function (err, list_books) {
      if (err) { return next(err); }
      //Successful, so render
      res.render('book_list', { title: 'Book List', book_list: list_books });
    });
```